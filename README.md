# API for order registration
> Tomas Viršičius

CRUD project, which implements simple order registration

### What does it do?
- Create order
- Update order details
- Get all orders
- Get order by id
- Delete order

### How to run?

```shell 
git clone https://github.com/Tomas-web/web-service.git
cd web-service
git submodule init
git submodule update
docker-compose build
docker-compose up
```

### SOAP requests
#### Get all orders
```shell 
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:os="http://spring.io/guides/gs-producing-web-service">
<soapenv:Header />
<soapenv:Body>
    <os:getOrdersRequest />
</soapenv:Body>
</soapenv:Envelope>
```

#### Get order by id
```shell 
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:os="http://spring.io/guides/gs-producing-web-service">
    <soapenv:Header />
    <soapenv:Body>
        <os:getOrderRequest>
            <os:id>2</os:id>
        </os:getOrderRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

#### Create order
```shell 
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:os="http://spring.io/guides/gs-producing-web-service">
<soapenv:Header />
<soapenv:Body>
    <os:createOrderRequest>
        <os:order>
            <os:itemName>Some name</os:itemName>
            <os:shopName>Some shop</os:shopName>
            <os:status>Some status</os:status>
            <os:user>
                <os:id>1234</os:id>
                <os:name>Hi</os:name>
                <os:surname>Surname</os:surname>
                <os:number>+37061234567</os:number>
                <os:email>email@gmail.com</os:email>
            </os:user>
        </os:order>
    </os:createOrderRequest>
</soapenv:Body>
</soapenv:Envelope>
```

#### Update order
```shell 
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:os="http://spring.io/guides/gs-producing-web-service">
<soapenv:Header />
<soapenv:Body>
    <os:updateOrderRequest>
        <os:id>3</os:id>
        <os:itemName>Some name updated</os:itemName>
        <os:shopName>Some shop updated</os:shopName>
        <os:status>Some status updated</os:status>
        <os:user>
            <os:id>1234</os:id>
        </os:user>
    </os:updateOrderRequest>
</soapenv:Body>
</soapenv:Envelope>
```

#### Delete order
```shell 
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:os="http://spring.io/guides/gs-producing-web-service">
<soapenv:Header />
<soapenv:Body>
    <os:deleteOrderRequest>
        <os:id>3</os:id>
    </os:deleteOrderRequest>
</soapenv:Body>
</soapenv:Envelope>
```

### Where are the docs?
> https://documenter.getpostman.com/view/13932194/TzJuAy4Z#intro
