package com.webservice.restservice.api.models;

import lombok.Data;

@Data
public class Order {

    private long id;
    private String itemName;
    private String shopName;
    private String status;
    private int userId;
}
