package com.webservice.restservice.api.models;

import lombok.Data;

@Data
public class User {

    private int id;
    private String name = null;
    private String surname = null;
    private String number = null;
    private String email = null;
}
