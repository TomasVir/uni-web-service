package com.webservice.restservice.api.models;

import lombok.Data;

@Data
public class OrderResponse {
    private long id;
    private String itemName;
    private String shopName;
    private String status;
    private User user = null;
}
