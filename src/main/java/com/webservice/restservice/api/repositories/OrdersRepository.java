package com.webservice.restservice.api.repositories;

import com.webservice.restservice.api.models.Order;
import com.webservice.restservice.api.models.OrderResponse;

import java.util.ArrayList;

public interface OrdersRepository {
    ArrayList<OrderResponse> getAll();
    OrderResponse getById(long id);
    OrderResponse createOrder(OrderResponse order);
    OrderResponse updateOrderDetails(long id, OrderResponse order);
    Boolean deleteOrder(long id);
}
