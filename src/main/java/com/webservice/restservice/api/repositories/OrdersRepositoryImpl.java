package com.webservice.restservice.api.repositories;

import com.webservice.restservice.api.exceptions.BadRequestException;
import com.webservice.restservice.api.exceptions.OrderDoesNotExistException;
import com.webservice.restservice.api.exceptions.UserNotFoundException;
import com.webservice.restservice.api.models.Order;
import com.webservice.restservice.api.models.OrderResponse;
import com.webservice.restservice.api.models.User;
import com.webservice.restservice.api.services.UsersRestService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class OrdersRepositoryImpl implements OrdersRepository {

    private final UsersRestService usersRestService;

    public ArrayList<Order> orders;

    private void setInitialOrders() {
        orders = new ArrayList<Order>();
        val order1 = new Order();
        order1.setId(1);
        order1.setItemName("Mouse");
        order1.setShopName("Skytech");
        order1.setStatus("On the way");
        order1.setUserId(12345);
        val order2 = new Order();
        order2.setId(2);
        order2.setItemName("Keyboard");
        order2.setShopName("Varle");
        order2.setStatus("Preparing");
        order2.setUserId(74638);
        val order3 = new Order();
        order3.setId(3);
        order3.setItemName("Monitor");
        order3.setShopName("Topo centras");
        order3.setStatus("On the way");
        order3.setUserId(11234);
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
    }

    @Override
    public ArrayList<OrderResponse> getAll() {
        if (orders == null) {
            setInitialOrders();
        }
        val ordersResponse = new ArrayList<OrderResponse>();
        for (val order : orders) {
            ordersResponse.add(toDTO(order));
        }
        return ordersResponse;
    }

    @Override
    public OrderResponse getById(long id) {
        if (orders == null) {
            setInitialOrders();
        }

        for (val order : orders) {
            if (order.getId() == id) {
                return toDTO(order);
            }
        }

        return null;
    }

    @Override
    public OrderResponse createOrder(OrderResponse order) {
        if (orders == null) {
            setInitialOrders();
        }

        if (!isOrderValid(order) || order.getUser() == null || order.getUser().getId() == 0 || !usersRestService.allFieldsValid(order.getUser())) {
            throw new BadRequestException();
        }

        long highestId = 0;

        for (val o : orders) {
            if (highestId < o.getId()) {
                highestId = o.getId();
            }
        }

        long newOrderId = highestId + 1;
        val o = new Order();
        o.setId(newOrderId);
        o.setUserId(order.getUser().getId());
        o.setStatus(order.getStatus());
        o.setShopName(order.getShopName());
        o.setItemName(order.getItemName());
        orders.add(o);
        order.setId(newOrderId);
        try {
            val user = usersRestService.getById(order.getUser().getId());
            order.setUser(user);
            return order;
        } catch (Exception ex) {
            val user = usersRestService.create(order.getUser());
            order.setUser(user);
            return order;
        }
    }

    @Override
    public OrderResponse updateOrderDetails(long id, OrderResponse order) {
        if (orders == null) {
            setInitialOrders();
        }

        if (!isOrderValid(order)) {
            throw new BadRequestException();
        }

        order.setId(id);

        for (val o : orders) {
            if (o.getId() == id) {
                int idx = orders.indexOf(o);
                val order1 = new Order();
                order1.setId(order.getId());
                order1.setItemName(order.getItemName());
                order1.setShopName(order.getShopName());
                order1.setStatus(order.getStatus());
                if (order.getUser() == null || order.getUser().getId() == 0) {
                    order1.setUserId(o.getUserId());
                } else {
                    order1.setUserId(order.getUser().getId());
                }
                orders.set(idx, order1);
                return toDTO(order1);
            }
        }

        return null;
    }

    @Override
    public Boolean deleteOrder(long id) {
        if (orders == null) {
            setInitialOrders();
        }


        for (val order : orders) {
            if (order.getId() == id) {
                orders.remove(order);
                return true;
            }
        }

        return false;

    }

    private Boolean isOrderValid(OrderResponse order) {
        return order.getItemName() != null && order.getShopName() != null && order.getStatus() != null;
    }

    private OrderResponse toDTO(Order order) {
        val orderResponse = new OrderResponse();
        orderResponse.setId(order.getId());
        orderResponse.setItemName(order.getItemName());
        orderResponse.setShopName(order.getShopName());
        orderResponse.setStatus(order.getStatus());
        try {
            val user = usersRestService.getById(order.getUserId());
            if (user != null) {
                orderResponse.setUser(user);
            } else {
                val userWithId = new User();
                userWithId.setId(order.getUserId());
                orderResponse.setUser(userWithId);
            }
            return orderResponse;
        } catch (Exception ex) {
            val userWithId = new User();
            userWithId.setId(order.getUserId());
            orderResponse.setUser(userWithId);
            return orderResponse;
        }
    }
}
