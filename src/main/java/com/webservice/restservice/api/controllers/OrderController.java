package com.webservice.restservice.api.controllers;

import com.webservice.restservice.api.exceptions.BadRequestException;
import com.webservice.restservice.api.exceptions.OrderDoesNotExistException;
import com.webservice.restservice.api.models.Order;
import com.webservice.restservice.api.models.OrderResponse;
import com.webservice.restservice.api.repositories.OrdersRepository;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;

@RestController
@RequestMapping(path = "/orders")
public class OrderController {

    private final OrdersRepository ordersRepository;

    public OrderController(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    @GetMapping
    public ResponseEntity<ArrayList<OrderResponse>> getAllOrders() {
//        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        //        oversized
//        PR
//        test
        try {
            val result = ordersRepository.getAll();
            if (result == null) {
                throw new Exception();
            } else {
                return ResponseEntity.ok(result);
            }
        } catch (Exception ex) {
            System.out.println("Unhandled exception" + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<OrderResponse> getOrderById(@PathVariable("id") long id) {
        try {
            val order = ordersRepository.getById(id);
            if (order == null) {
                throw new OrderDoesNotExistException();
            } else {
                return ResponseEntity.ok(order);
            }
        } catch (OrderDoesNotExistException ex) {
            System.out.println("Order does not exist" + ex);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception ex) {
            System.out.println("Unhandled exception" + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<OrderResponse> createOrder(@RequestBody OrderResponse order) {
        try {
            val result = ordersRepository.createOrder(order);

            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(result.getId())
                    .toUri();
            return ResponseEntity.created(location).body(result);
        } catch (BadRequestException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception ex) {
            System.out.println("Unhandled exception" + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<OrderResponse> updateOrderDetails(
            @PathVariable("id") long id,
            @RequestBody OrderResponse order) {
        try {
            val result = ordersRepository.updateOrderDetails(id, order);
            if (result == null) {
                throw new OrderDoesNotExistException();
            } else {
                return ResponseEntity.ok(result);
            }
        } catch (BadRequestException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (OrderDoesNotExistException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception ex) {
            System.out.println("Unhandled exception" + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<OrderResponse> deleteOrder(@PathVariable("id") long id) {
        try {
            Boolean val = ordersRepository.deleteOrder(id);
            if (val) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                throw new OrderDoesNotExistException();
            }
        } catch (OrderDoesNotExistException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception ex) {
            System.out.println("Unhandled exception" + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
