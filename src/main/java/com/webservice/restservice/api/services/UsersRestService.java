package com.webservice.restservice.api.services;

import com.webservice.restservice.api.exceptions.BadRequestException;
import com.webservice.restservice.api.exceptions.UnhandledException;
import com.webservice.restservice.api.exceptions.UserNotFoundException;
import com.webservice.restservice.api.models.User;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class UsersRestService {

    private final RestTemplate restTemplate;


    public UsersRestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public User[] getAll() {
        String url = "http://contacts:5000/contacts";
        ResponseEntity<User[]> response = this.restTemplate.getForEntity(url, User[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            throw new UserNotFoundException();
        }
    }

    public User getById(int id) {
        try {
            String url = "http://contacts:5000/contacts/{id}";
            ResponseEntity<User> response = this.restTemplate.getForEntity(url, User.class, id);
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                throw new UserNotFoundException();
            }
        } catch (HttpStatusCodeException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new UserNotFoundException();
            } else {
                throw new UnhandledException();
            }
        }
    }

    public User create(User user) {
        if (!allFieldsValid(user)) {
            throw new BadRequestException();
        }

        if (user.getId() == 0) {
            throw new BadRequestException();
        }

        String url = "http://contacts:5000/contacts";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, Object> map = new HashMap<>();
        map.put("id", user.getId());
        map.put("name", user.getName());
        map.put("number", user.getNumber());
        map.put("surname", user.getSurname());
        map.put("email", user.getEmail());

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        try {
            ResponseEntity<String> response = this.restTemplate.postForEntity(url, entity, String.class);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                return user;
            } else {
                throw new BadRequestException();
            }
        } catch (BadRequestException ex) {
            throw ex;
        } catch (HttpStatusCodeException ex) {
            if (ex.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new BadRequestException();
            } else {
                throw new UnhandledException();
            }
        }
    }

    public User update(int id, User user) {
        if (!allFieldsValid(user)) {
            throw new BadRequestException();
        }

        try {
            User u = getById(id);
            if (u == null) {
                throw new UserNotFoundException();
            }

            String url = "http://contacts:5000/contacts/{id}";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            HttpEntity<User> entity = new HttpEntity<>(user, headers);
            ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, id);

            if (response.getStatusCode() == HttpStatus.OK) {
                user.setId(id);
                return user;
            } else if (response.getStatusCode() == HttpStatus.BAD_REQUEST){
                throw new BadRequestException();
            } else {
                throw new UserNotFoundException();
            }
        } catch (BadRequestException | UserNotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new UnhandledException();
        }
    }

    public ResponseEntity<HttpStatus> delete(int id) {
        String url = "http://contacts:5000/contacts/{id}";

        try {
            this.restTemplate.delete(url, id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (HttpStatusCodeException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    public Boolean allFieldsValid(User user) {
        return user.getEmail() != null && user.getName() != null && user.getNumber() != null && user.getSurname() != null;
    }
}
