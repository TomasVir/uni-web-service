package com.webservice.restservice.api.soap.exceptions;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.CUSTOM,
        customFaultCode = "{" + OrderNotFoundException.NAMESPACE_URI + "}custom_fault",
        faultStringOrReason = "Order not found")
public class OrderNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;
    public static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    public OrderNotFoundException(String message) {
        super(message);
    }
}
