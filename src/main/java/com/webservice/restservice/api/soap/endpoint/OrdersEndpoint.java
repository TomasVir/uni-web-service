package com.webservice.restservice.api.soap.endpoint;

import com.webservice.restservice.api.soap.exceptions.OrderNotFoundException;
import com.webservice.restservice.api.soap.service.OrdersService;
import io.spring.guides.gs_producing_web_service.CreateOrderRequest;
import io.spring.guides.gs_producing_web_service.CreateOrderResponse;
import io.spring.guides.gs_producing_web_service.DeleteOrderRequest;
import io.spring.guides.gs_producing_web_service.DeleteOrderResponse;
import io.spring.guides.gs_producing_web_service.GetOrderRequest;
import io.spring.guides.gs_producing_web_service.GetOrderResponse;
import io.spring.guides.gs_producing_web_service.GetOrdersRequest;
import io.spring.guides.gs_producing_web_service.GetOrdersResponse;
import io.spring.guides.gs_producing_web_service.UpdateOrderRequest;
import io.spring.guides.gs_producing_web_service.UpdateOrderResponse;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class OrdersEndpoint {

    @Autowired
    private OrdersService ordersService;

    @PayloadRoot(namespace = "http://spring.io/guides/gs-producing-web-service", localPart = "getOrderRequest")
    @ResponsePayload
    public GetOrderResponse getOrderRequest(@RequestPayload GetOrderRequest request) throws OrderNotFoundException {
        val response = new GetOrderResponse();
        try {
            val order = ordersService.getById(request.getId());
            response.setOrder(order);
            return response;
        } catch (Exception ex) {
            throw new OrderNotFoundException("Order not found");
        }
    }

    @PayloadRoot(namespace = "http://spring.io/guides/gs-producing-web-service", localPart = "getOrdersRequest")
    @ResponsePayload
    public GetOrdersResponse getOrdersRequest(@RequestPayload GetOrdersRequest request) {
        val response = new GetOrdersResponse();
        val orders = ordersService.getAll(request.getId());
        for (val order : orders) {
            response.getOrder().add(order);
        }
        return response;
    }

    @PayloadRoot(namespace = "http://spring.io/guides/gs-producing-web-service", localPart = "createOrderRequest")
    @ResponsePayload
    public CreateOrderResponse createOrderRequest(@RequestPayload CreateOrderRequest request) {
        val response = new CreateOrderResponse();
        response.setOrder(ordersService.create(request.getOrder()));
        return response;
    }

    @PayloadRoot(namespace = "http://spring.io/guides/gs-producing-web-service", localPart = "deleteOrderRequest")
    @ResponsePayload
    public DeleteOrderResponse deleteOrderRequest(@RequestPayload DeleteOrderRequest request) throws OrderNotFoundException {
        val response = new DeleteOrderResponse();
        val res = ordersService.delete(request.getId());
        if (res == null) {
            throw new OrderNotFoundException("Order not found");
        }
        response.setMessage(res);
        return response;
    }

    @PayloadRoot(namespace = "http://spring.io/guides/gs-producing-web-service", localPart = "updateOrderRequest")
    @ResponsePayload
    public UpdateOrderResponse updateOrderRequest(@RequestPayload UpdateOrderRequest request) throws OrderNotFoundException {
        val response = new UpdateOrderResponse();
        try {
            response.setOrder(ordersService.update(request));
            return response;
        } catch (Exception ex) {
            throw new OrderNotFoundException("Order not found");
        }
    }
}
