package com.webservice.restservice.api.soap.service;

import com.webservice.restservice.api.exceptions.OrderDoesNotExistException;
import com.webservice.restservice.api.models.OrderResponse;
import com.webservice.restservice.api.repositories.OrdersRepository;
import com.webservice.restservice.api.soap.exceptions.OrderNotFoundException;
import io.spring.guides.gs_producing_web_service.CreateOrderRequest;
import io.spring.guides.gs_producing_web_service.Order;
import io.spring.guides.gs_producing_web_service.OrderWithoutId;
import io.spring.guides.gs_producing_web_service.UpdateOrderRequest;
import io.spring.guides.gs_producing_web_service.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrdersService {

    private final OrdersRepository ordersRepository;

    public Order getById(long id) {
        val order = ordersRepository.getById(id);

        return toDTO(order);
    }

    public List<Order> getAll(Long id) {
        List<OrderResponse> orders = new ArrayList<OrderResponse>();
        if (id != null) {
            val o = ordersRepository.getById(id);
            if (o != null) {
                orders.add(o);
            }
        } else {
            orders = ordersRepository.getAll();
        }

        List<Order> ordersDTO = new ArrayList<Order>();
        for (val order : orders) {
            ordersDTO.add(toDTO(order));
        }
        return ordersDTO;
    }

    public Order create(OrderWithoutId order) {
        val o = new OrderResponse();
        o.setItemName(order.getItemName());
        o.setStatus(order.getStatus());
        o.setShopName(order.getShopName());
        o.setUser(toUserDTO(order.getUser()));
        val createdOrder = ordersRepository.createOrder(o);
        return toDTO(createdOrder);
    }

    public Order update(UpdateOrderRequest order) {
        val dataToPass = new OrderResponse();
        dataToPass.setId(order.getId());
        dataToPass.setItemName(order.getItemName());
        dataToPass.setShopName(order.getShopName());
        dataToPass.setStatus(order.getStatus());
        val user = new com.webservice.restservice.api.models.User();
        user.setId(order.getUser().getId());
        dataToPass.setUser(user);
        val updatedOrder = ordersRepository.updateOrderDetails(order.getId(), dataToPass);
        return toDTO(updatedOrder);
    }

    public String delete(long id) {
        val isDeleted = ordersRepository.deleteOrder(id);
        if (isDeleted) {
            return "Order deleted successfully";
        } else {
            return null;
        }
    }

    private OrderResponse toOrderResponseDTO(Order order) {
        val dto = new OrderResponse();
        dto.setId(order.getId());
        dto.setUser(toUserDTO(order.getUser()));
        dto.setStatus(order.getStatus());
        dto.setShopName(order.getShopName());
        dto.setItemName(order.getItemName());
        return dto;
    }

    private com.webservice.restservice.api.models.User toUserDTO(User user) {
        val dto = new com.webservice.restservice.api.models.User();
        dto.setId(user.getId());
        dto.setSurname(user.getSurname());
        dto.setName(user.getName());
        dto.setNumber(user.getNumber());
        dto.setEmail(user.getEmail());
        return dto;
    }

    private User toSoapUserDTO(com.webservice.restservice.api.models.User user) {
        val dto = new User();
        dto.setId(user.getId());
        dto.setSurname(user.getSurname());
        dto.setName(user.getName());
        dto.setNumber(user.getNumber());
        dto.setEmail(user.getEmail());
        return dto;
    }


    private Order toDTO(com.webservice.restservice.api.models.OrderResponse order) {
        val dto = new Order();
        dto.setId(order.getId());
        dto.setItemName(order.getItemName());
        dto.setShopName(order.getShopName());
        dto.setStatus(order.getStatus());
        dto.setUser(toSoapUserDTO(order.getUser()));
        return dto;
    }
}
